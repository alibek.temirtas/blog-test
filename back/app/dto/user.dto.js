class UserDTO {
    constructor(_user) {
        this._id = _user._id;
        this.phoneNumber = _user.phoneNumber;
        this.profile = _user.profile;
        this.role = _user.role;
    }
}

module.exports = UserDTO;
