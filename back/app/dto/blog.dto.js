class BlogDTO {
    constructor(_blog) {
        this._id = _blog._id;
        this.author = _blog.author;
        this.title = _blog.title;
        this.desc = _blog.desc;
        this.status = _blog.status;
        this.img = _blog.img;
        this.visits = _blog.visits.length;
        this.likes = _blog.likes.length;
        this.createdDate = _blog.createdDate;
        this.deletedDate = _blog.deletedDate;
        this.isDeleted = _blog.isDeleted;
        this.filePath = _blog.filePath;
    }

    static makeArrayDTO(_blogs) {
        return _blogs.map((blog) => {
            return {
                _id: blog._id,
                author: blog.author,
                title: blog.title,
                desc: blog.desc,
                status: blog.status,
                img: blog.img,
                visits: blog.visits.length,
                likes: blog.likes.length,
                createdDate: blog.createdDate,
                deletedDate: blog.deletedDate,
                isDeleted: blog.isDeleted,
                filePath: blog.filePath
            }
        });
    }
}

module.exports = BlogDTO;
