class CommentDto {
    constructor(_comment) {
        this._id = _comment._id;
        this.author = _comment.author;
        this.userId = _comment.userId;
        this.blogId = _comment.blogId;
        this.text = _comment.text;
        this.createdDate = _comment.createdDate;
    }

    static makeArrayDTO(_comments) {
        return _comments.map((comment) => {
            return {
                _id: comment._id,
                author: comment.author,
                userId: comment.userId,
                blogId: comment.blogId,
                text: comment.text,
                createdDate: comment.createdDate
            }
        });
    }
}

module.exports = CommentDto;
