const express = require('express');
const router = express.Router();
const expressValidator = require('express-validator');

const config = require('../_constants/config');
const eventTypes = require('../_constants/eventTypes');
const ipConnection = require('../_helpers/network');
const verifyToken = require('../_jwt/verifyToken');

const LogModel = require('../models/log.model');
const CommentModel = require('../models/comment.model');
const UserModel = require('../models/user.model');

// DTO
const CommentDto = require('../dto/comment.dto');

router.use(expressValidator());

// POST create comment
router.post('/', verifyToken, (req, res) => {

    console.log('userId: ', req.userId);
    const userId = req.userId;
    // Validation :
    req.checkBody('text', 'text is required').notEmpty();
    req.checkBody('blogId', 'blogId is required').notEmpty();

    let errors = req.validationErrors();

    if (errors) {
        res.status(config.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    UserModel.findById(userId, (err, user) => {
        if (err) return res.status(config.SERVER_ERROR).send({error: 'server error'});
        if (!user) return res.status(config.NOT_FOUND).send({error: 'user not found'});

        let comment = new CommentModel({
            userId: userId,
            author: user.profile,
            text: req.body.text,
            blogId: req.body.blogId
        });

        comment.create((err, result) => {
            if (err) {
                res.status(config.SERVER_ERROR).send({error: 'comment create error.'});
                return;
            }

            // logging
            let log = new LogModel({
                userId: userId,
                eventType: eventTypes.CREATE_COMMENT,
                IPAdress: ipConnection.getIp(req),
                desc: 'Пользователь создал комментарий'
            });

            log.create((err, result) => {
                if (err) console.log('error in logging...');
            });

            req.io.to(comment.blogId).emit('comments', new CommentDto(comment));
            res.status(config.SUCCESS).send(new CommentDto(comment));
        });
    });

});

// GET blog by blogId
router.get('/:id', verifyToken, (req, res) => {
    const blogId = req.params.id;
    CommentModel.getCommentsByBlogId(blogId, (err, comments) => {
        if (err) return res.status(config.SERVER_ERROR).send({error: 'server error'});
        if (!comments) return res.status(config.NOT_FOUND).send({error: 'blogs not found'});

        res.status(config.SUCCESS).send(CommentDto.makeArrayDTO(comments));
    });

});

router.delete('/', verifyToken, (req, res) => {

    let errors = req.validationErrors();

    if (errors) {
        res.status(config.SERVER_ERROR).send({errors: errors});
        return false;
    }

    CommentModel.deleteCommentById(req.body.comment._id, (err, result) => {
        if (err) return res.status(config.SUCCESS).send("There was a problem deleting comment.");

        req.io.to(req.body.comment.blogId).emit('deleteComment', req.body.comment._id);
        res.status(config.SUCCESS).send({msg: 'Comment has been deleted.'});
    });
});


module.exports = router;
