const express = require('express');
const router = express.Router();
const expressValidator = require('express-validator');
const multer = require('multer');
const fs = require('fs');

const config = require('../_constants/config');
const verifyToken = require('../_jwt/verifyToken');

const FileModel = require('../models/file.model');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __absolutePath + '/files/')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '-' + file.originalname)
    }
});

const upload = multer({storage: storage});

router.use(expressValidator());

// POST upload file
router.post('/', verifyToken, upload.single('file'), (req, res) => {
    console.log('req.file: ', req.file);
    console.log('req.body: ', req.body);

    let file = new FileModel({
        name: req.file.filename,
        size: req.file.size,
        type: req.file.mimetype,
        path: '/file/' + req.file.filename
    });

    file.create((err, result)=>{
        if (err) {
            console.log('Create user method has been failed.');
            res.status(config.SERVER_ERROR).send({error: 'file upload error.'});
            return;
        }

        res.status(config.SUCCESS).send(file);
    });
});

// get image by name
router.get('/:filename', (req, res) => {
    let filePath = __absolutePath+'/files/'+req.params.filename;

    fs.stat(filePath, (err, stat) => {
        if(err == null) {
            res.download(filePath);
        }  else {
            res.status(config.NOT_FOUND).send({error: 'file not found'});
        }
    });
});



module.exports = router;
