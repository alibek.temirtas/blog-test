const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const expressValidator = require('express-validator');
const store = require('store');

const eventTypes = require('../_constants/eventTypes');
const ipConnection = require('../_helpers/network');
const smsApi = require('../_helpers/smsApi');
const generateToken = require('../_jwt/generateToken');

// CONSTANTS :
const roles = require('../_constants/roles');
const config = require('../_constants/config');

const LogModel = require('../models/log.model');
const UserModel = require('../models/user.model');

const UserDTO = require('../dto/user.dto');

router.use(expressValidator());

// REGISTER USER :
router.post('/register', (req, res) => {

    // Validation :
    req.checkBody('phoneNumber', 'phone number is required').notEmpty();
    req.checkBody('surname', 'surname is required').notEmpty();
    req.checkBody('name', 'name is required').notEmpty();
    req.checkBody('phoneNumber', 'phone number should be integer').isInt();
    req.checkBody('password', 'password is required').notEmpty();
    let errors = req.validationErrors();

    if (errors) {
        res.status(config.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    UserModel.isExist(req.body.phoneNumber, (error, result) => {
        if (result) {
            res.status(config.SERVER_ERROR).send({
                error: "This phone number has been already registered."
            });
            return;
        }

        // Create user :
        let hashedPassword = bcrypt.hashSync(req.body.password, 8);

        let user = {
            phoneNumber: req.body.phoneNumber,
            password: hashedPassword,
            profile: {
                name: req.body.name || null,
                surname: req.body.surname || null
            }
        };

        let generatedCode = Math.floor(1000 + Math.random() * 9000);
        user.generatedCode = generatedCode;

        console.log("verify code has been generated", generatedCode);

        store.set(user.phoneNumber + '_SMS_CODE', JSON.stringify(user));

        // res.status(config.SUCCESS).send({msg: 'Please, check your phone. Enter sms code.'});
        sendSms(user, res);
    });

});

// ACTIVATE AFTER REGISTRATION :
router.post('/activate', (req, res) => {

    // Validation :
    req.checkBody('phoneNumber', 'phone number is required').notEmpty();
    req.checkBody('phoneNumber', 'phone number should be integer').isInt();
    req.checkBody('activateCode', 'activate code is required').notEmpty();
    req.checkBody('activateCode', 'activate code should be integer').isInt();
    let errors = req.validationErrors();

    if (errors) {
        res.status(config.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    let formData = store.get(req.body.phoneNumber + '_SMS_CODE');

    if (!formData) {
        console.log('no data in store');
        res.status(config.SERVER_ERROR).send({error: 'no data in store'});
        return;
    }

    let parsedData = JSON.parse(formData);

    console.log('parsedData: ', parsedData);

    if (!(req.body.activateCode == parsedData.generatedCode)) {
        console.log('Sms code is not valid !');
        res.status(config.SERVER_ERROR).send({error: 'Sms code is not valid !'});
        return;
    }

    delete parsedData.generatedCode;

    parsedData.role = roles.USER;
    let user = new UserModel(parsedData);

    user.create((err, result) => {
        if (err) {
            console.log('Create user method has been failed.');
            res.status(config.SERVER_ERROR).send({error: 'User register error.'});
            return;
        }

        // logging
        let log = new LogModel({
            userId: user._id,
            eventType: eventTypes.REGISTER,
            IPAdress: ipConnection.getIp(req),
            desc: 'Пользователь зарегистрировался'
        });

        log.create((err, result) => {
            if (err) console.log('error in logging...');
        });

        store.remove(req.body.phoneNumber + '_SMS_CODE');

        if (user) {
            console.log('User is activated');
            res.status(config.SUCCESS).send({msg: 'User is activated'});
        } else {
            console.log('user not found to modified isActivated');
            res.status(config.SERVER_ERROR).send({error: 'user not found to modified isActivated'});
        }
    });

});

// LOGIN USER :
router.post('/login', (req, res) => {

    // Validation :
    req.checkBody('phoneNumber', 'phone number is required').notEmpty();
    req.checkBody('phoneNumber', 'phone number should be integer').isInt();
    req.checkBody('password', 'password is required').notEmpty();
    let errors = req.validationErrors();

    if (errors) {
        res.status(config.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    UserModel.isExist(req.body.phoneNumber, (err, result) => {

        // Login :
        if (err) return res.status(config.SERVER_ERROR).send({error: 'Error on the server.'});
        if (!result) return res.status(config.NOT_FOUND).send({msg: 'User not found.'});

        logIn(result);
    });

    function logIn(user) {
        let passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
        if (!passwordIsValid) return res.status(config.FORBIDDEN).send({auth: false, token: null});

        let token = generateToken(user);

        console.log('logged in Our System');
        if (user.role == roles.ADMIN) {
            res.status(config.SUCCESS).send({user: new UserDTO(user), token: token});
        } else {
            res.status(config.SUCCESS).send({user: new UserDTO(user), token: token});
        }

        let log = new LogModel({
            userId: user._id,
            eventType: eventTypes.AUTH,
            IPAdress: ipConnection.getIp(req),
            desc: 'Пользователь авторизовался'
        });

        log.create((err, result) => {
            if (err) console.log('error in logging...');
        });

    }

});


// SEND SMS CODE TO USER'S PHONE NUMBER :
function sendSms(user, res) {
    let formDataSmsCode = {
        phoneNumber: user.phoneNumber,
        message: 'Vash kod podtverzhdeniya: ' + user.generatedCode
    };

    smsApi.sendHttpSms(formDataSmsCode, (err, result) => {
        if (err) {
            console.log('Error sms code server can not send.', err.error);
            res.status(config.SERVER_ERROR).send(err.error);
            return;
        }

        res.status(config.SUCCESS).send({msg: 'Please, check your phone. Enter sms code.'});
    });
}

module.exports = router;
