const express = require('express');
const router = express.Router();
const expressValidator = require('express-validator');
const ObjectID = require('mongodb').ObjectID;

const roles = require('../_constants/roles');
const blogStatus = require('../_constants/blog_status');
const config = require('../_constants/config');
const eventTypes = require('../_constants/eventTypes');
const ipConnection = require('../_helpers/network');
const verifyToken = require('../_jwt/verifyToken');

const LogModel = require('../models/log.model');
const BlogModel = require('../models/blog.model');

// DTO
const BlogDTO = require('../dto/blog.dto');

router.use(expressValidator());

// GET blogs :
router.get('/', (req, res) => {
    const perPage = config.PER_PAGE;
    let page = +req.query.page || 1;

    let query = {
        perPage: perPage,
        page: page
    };

    BlogModel.getAllBlogs(query,(err, blogs) => {
        if (err) return res.status(config.SERVER_ERROR).send({error: 'server error'});
        if (!blogs) return res.status(config.NOT_FOUND).send({error: 'blog not found'});

        BlogModel.getCount({status: blogStatus.APPROVED, isDeleted: false}).then(count=>{
            let pages = Math.ceil(count / perPage);

            res.status(config.SUCCESS).send({
                blogs: BlogDTO.makeArrayDTO(blogs),
                pages,
                count
            });
        });
    });
});

router.get('/admin', verifyToken, (req, res) => {
    const perPage = config.PER_PAGE;
    let page = +req.query.page || 1;

    let query = {
        perPage: perPage,
        page: page
    };

    BlogModel.getAllBlogsForAdmin(query,(err, blogs) => {
        if (err) return res.status(config.SERVER_ERROR).send({error: 'server error'});
        if (!blogs) return res.status(config.NOT_FOUND).send({error: 'blog not found'});

        BlogModel.getCount({}).then(count=>{
            let pages = Math.ceil(count / perPage);

            res.status(config.SUCCESS).send({
                blogs: BlogDTO.makeArrayDTO(blogs),
                pages,
                count
            });
        });
    });
});


// GET blogs by userId
router.get('/byuser', verifyToken, (req, res) => {
    const perPage = config.PER_PAGE;
    let page = +req.query.page || 1;

    let query = {
        perPage: perPage,
        page: page
    };

    BlogModel.getBlogsByUserId(req.userId, query, (err, blogs) => {
        if (err) return res.status(config.SERVER_ERROR).send({error: 'server error'});
        if (!blogs) return res.status(config.NOT_FOUND).send({error: 'blog not found'});

        BlogModel.getCount({userId: ObjectID(req.userId), isDeleted: false}).then(count=>{
            let pages = Math.ceil(count / perPage);

            res.status(config.SUCCESS).send({
                blogs: BlogDTO.makeArrayDTO(blogs),
                pages,
                count
            });
        });
    });

});

// POST create blog
router.post('/', verifyToken, (req, res) => {

    console.log('req: ', req.body);

    // Validation :
    req.checkBody('userId', 'userId is required').notEmpty();
    req.checkBody('author', 'author is required').notEmpty();
    req.checkBody('title', 'title is required').notEmpty();
    req.checkBody('desc', 'desc is required').notEmpty();

    let errors = req.validationErrors();

    if (errors) {
        res.status(config.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    let blog = new BlogModel({
        userId: req.body.userId,
        profile: req.body.author,
        title: req.body.title,
        desc: req.body.desc,
        filePath: req.body.filePath
    });

    blog.create((err, result) => {
        if (err) {
            res.status(config.SERVER_ERROR).send({error: 'blog create error.'});
            return;
        }

        // logging
        let log = new LogModel({
            userId: req.body.userId,
            eventType: eventTypes.REGISTER,
            IPAdress: ipConnection.getIp(req),
            desc: 'Пользователь создал блог'
        });

        log.create((err, result) => {
            if (err) console.log('error in logging...');
        });


        res.status(config.SUCCESS).send(new BlogDTO(blog));
    });

});

// PUT update blog
router.put('/', verifyToken, (req, res) => {
    console.log('req.role === roles.ADMIN: ', req.role === roles.ADMIN);

    // Validation :
    req.checkBody('_id', '_id is required').notEmpty();

    let errors = req.validationErrors();

    if (errors) {
        res.status(config.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    let formData = {
        title: req.body.title,
        desc: req.body.desc,
        isDeleted: req.body.isDeleted || false,
        status: req.role === roles.ADMIN ? req.body.status : blogStatus.MODERATION
    };

    req.body.status = formData.status;

    BlogModel.findByIdAndUpdate(req.body._id, formData, (err, result) => {
        if (err) return res.status(config.SERVER_ERROR).send("There was a problem update blog.");

        res.status(config.SUCCESS).send(req.body);
    });

});

// GET blog by blogId
router.get('/detail/:id', verifyToken, (req, res) => {
    const blogId = req.params.id;

    BlogModel.findById(blogId, (err, blog) => {
        if (err) return res.status(config.SERVER_ERROR).send({error: 'server error'});
        if (!blog) return res.status(config.NOT_FOUND).send({error: 'blog not found'});

        let _visits = blog.visits;
        let filteredVisits = _visits.filter(item => item.userToken === req.userToken);

        if (filteredVisits.length > 0) {
            res.status(config.SUCCESS).send(new BlogDTO(blog));
        } else {
            BlogModel.addVisiter(blogId, {userToken: req.userToken}, (er, result) => {
                if (er) return res.status(config.SERVER_ERROR).send({error: 'add visit server error'});

                if (result.result.nModified > 0) {
                    console.log('Visit has been added.');

                    res.status(config.SUCCESS).send(new BlogDTO(blog));
                } else {
                    console.log('blog not found to modified visits');
                    res.status(config.SERVER_ERROR).send({error: 'blog not found to modified visits'});
                }
            });

        }
    });

});

router.post('/addlike', verifyToken, (req, res) => {

    // Validation :
    req.checkBody('blogId', 'blogId is required').notEmpty();
    req.checkBody('userId', 'approved is required').notEmpty();

    let errors = req.validationErrors();

    if (errors) {
        res.status(config.SERVER_ERROR).send({errors: errors});
        return false;
    }

    console.log('blogId: ', req.blogId);
    console.log('userId: ', req.userId);

    res.status(config.SUCCESS).send({msg: 'Add like'});
});

router.delete('/', verifyToken, (req, res) => {

    // Validation :
    req.checkBody('blogId', 'blogId is required').notEmpty();

    let errors = req.validationErrors();

    if (errors) {
        res.status(config.SERVER_ERROR).send({errors: errors});
        return false;
    }

    BlogModel.deleteBlogById(req.body.blogId, {isDeleted: true}, (err, result) => {
        if (err) return res.status(config.SUCCESS).send("There was a problem deleting blog.");

        res.status(config.SUCCESS).send({msg: 'Blog has been deleted.'});

    });
});


module.exports = router;
