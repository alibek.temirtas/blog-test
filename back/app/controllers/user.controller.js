const express = require('express');
const router = express.Router();
const expressValidator = require('express-validator');
const bcrypt = require('bcryptjs');

const config = require('../_constants/config');
const roles = require('../_constants/roles');
const verifyToken = require('../_jwt/verifyToken');
const UserModel = require('../models/user.model');

// DTO
const UserDTO = require('../dto/user.dto');

router.use(expressValidator());

// GET profile :
router.get('/profile', verifyToken, (req, res) => {

    UserModel.findById(req.userId, (err, user) => {
        if (err) return res.status(config.SERVER_ERROR).send({error: 'server error'});
        if (!user) return res.status(config.NOT_FOUND).send({error: 'user not found'});

        res.status(config.SUCCESS).send({
            user: new UserDTO(user)
        });
    });

});

// GET users list
router.get('/all', verifyToken, (req, res) => {

    const perPage = config.PER_PAGE;
    let page = +req.query.page || 1;

    let query = {
        perPage: perPage,
        page: page
    };

    UserModel.getUsers(query,(err, users) => {
        if (err) return res.status(config.SERVER_ERROR).send({error: 'server error'});
        if (!users) return res.status(config.NOT_FOUND).send({error: 'user not found'});

        UserModel.getCount().then(count=>{
            let pages = Math.ceil(count / perPage);

            res.status(config.SUCCESS).send({
                users,
                pages,
                count
            });
        });
    });

});

// PUT update user
router.put('/', verifyToken, (req, res) => {
    console.log('req: ', req.body);

    // Validation :
    req.checkBody('_id', '_id is required').notEmpty();

    let errors = req.validationErrors();

    if (errors) {
        res.status(config.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    let formData = {
        isDeleted: req.body.isDeleted || false,
        profile: {
            name: req.body.profile.name,
            surname: req.body.profile.surname
        }
    };

    UserModel.findByIdAndUpdate(req.body._id, formData, (err, result) => {
        if (err) return res.status(config.SERVER_ERROR).send("There was a problem update user.");

        res.status(config.SUCCESS).send(req.body);
    });

});


router.delete('/', verifyToken, (req, res) => {

    // Validation :
    req.checkBody('userId', 'userId is required').notEmpty();

    let errors = req.validationErrors();

    if (errors) {
        res.status(config.SERVER_ERROR).send({errors: errors});
        return false;
    }

    UserModel.deleteUserById(req.body.userId, {isDeleted: true}, (err, result) => {
        if (err) return res.status(config.SUCCESS).send("There was a problem deleting user.");

        res.status(config.SUCCESS).send({msg: 'User has been deleted.'});

    });
});


router.post('/', verifyToken, (req, res) => {

    // Validation :
    req.checkBody('phoneNumber', 'phone number is required').notEmpty();
    req.checkBody('surname', 'surname is required').notEmpty();
    req.checkBody('name', 'name is required').notEmpty();
    req.checkBody('phoneNumber', 'phone number should be integer').isInt();
    req.checkBody('password', 'password is required').notEmpty();
    let errors = req.validationErrors();

    if (errors) {
        res.status(config.REQUIRE_PARAMS).send({errors: errors});
        return false;
    }

    UserModel.isExist(req.body.phoneNumber, (error, result) => {
        if (result) {
            res.status(config.SERVER_ERROR).send({
                error: "This phone number has been already registered."
            });
            return;
        }

        // Create user :
        let hashedPassword = bcrypt.hashSync(req.body.password, 8);

        let user = new UserModel({
            phoneNumber: req.body.phoneNumber,
            password: hashedPassword,
            profile: {
                name: req.body.name || null,
                surname: req.body.surname || null
            },
            role: req.body.role || roles.USER
        });

        user.create((err, result) => {
            if (err) {
                console.log('Create user method has been failed.');
                res.status(config.SERVER_ERROR).send({error: 'User register error.'});
                return;
            }

            res.status(config.SUCCESS).send(new UserDTO(user));
        });
    });

});

module.exports = router;
