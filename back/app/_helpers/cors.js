const config = require('../_constants/config');

function getAccessControl(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://'+config.CLIENT_URL);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
}

module.exports = {
    getAccessControl: getAccessControl
};
