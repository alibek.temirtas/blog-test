const request = require('request');

const config = require('../_constants/config');

function sendHttpSms(params, cb) {

    let formData = {
        login: config.SMS_ACCOUNT_LOGIN,
        psw: config.SMS_ACCOUNT_PWD,
        phones: params.phoneNumber,
        mes: params.message,
        charset: 'utf-8',
        fmt: 3
    };

    let options = '';
    for (let key in formData) {
        options += key + '=' + formData[key] + '&';
    }

    console.log('options: ', config.SMS_URL + '/sys/send.php?' + options);
    request(config.SMS_URL + '/sys/send.php?' + options, (error, response) => {
        if (error) {
            console.log('Error to send sms :', error);
            return;
        }

        if (response && response.statusCode) {
            console.log('Sms has been successfully sent.');
            cb(error, response && response.statusCode);
        }

    });
}

module.exports = {
    sendHttpSms: sendHttpSms
};
