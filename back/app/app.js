const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');

const CORS =require('./_helpers/cors');

const app = express();
const io = require('socket.io')(4300);

global.__absolutePath = path.resolve('static');

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(CORS.getAccessControl);

// CONTROLLERS :
const authController = require('./controllers/auth.controller');
const userController = require('./controllers/user.controller');
const blogController = require('./controllers/blog.controller');
const commentController = require('./controllers/comment.controller');
const fileController = require('./controllers/file.controller');

app.use((req, res, next) => {
    req.io = io;
    next();
});

io.on('connection', (socket) => {
    console.log('a user connected');

    socket.on('blog', (data) => {
        console.log('connected.', data);
        socket.join(data.blogId);
    });


    socket.on('disconnect', () => {
        console.log(`Socket ${socket.id} disconnected.`);
    });

});


app.get('/api', (req, res) => {
    res.send('Blog web api is running');
});

app.use('/api', authController);
app.use('/api/user', userController);
app.use('/api/blog', blogController);
app.use('/api/comment', commentController);
app.use('/api/file', fileController);

module.exports = app;
