const jwt = require('jsonwebtoken');
const config = require('../_constants/config');

function verifyToken(req, res, next) {
    let token = req.headers['authorization'].split(' ')[1];

    if (!token) return res.status(config.FORBIDDEN).send({auth: false, msg: 'No token provided.'});

    jwt.verify(token, config.SECRET_KEY, (err, decoded) => {
        if (err) return res.status(config.SERVER_ERROR).send({auth: false, msg: 'Failed to authenticate token.'});

        req.userId = decoded.id;
        req.userToken = token;
        req.role = decoded.role;
        next();
    });
}

module.exports = verifyToken;
