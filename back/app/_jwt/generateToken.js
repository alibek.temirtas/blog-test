const jwt = require('jsonwebtoken');
const config = require('../_constants/config');

function generateToken(user) {
    return jwt.sign({id: user._id, role: user.role}, config.SECRET_KEY, {
        expiresIn: 86400 // expires in 24 hours
    });
}

module.exports = generateToken;
