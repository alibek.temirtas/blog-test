const ObjectID = require('mongodb').ObjectID;
const db = require('../db');
const moment = require('moment');

const DbEntity = require('./dbentity.model');

class CommentModel extends DbEntity {
    constructor(obj) {
        super();
        this.userId = ObjectID(obj.userId);
        this.author = obj.author;
        this.blogId = ObjectID(obj.blogId);
        this.text = obj.text;
    }

    create(cb) {
        db.get().collection('comments').insertOne(this, (err, result) => {
            cb(err, result);
        });
    }

    static getAllComments(cb) {
        db.get().collection('comments').find().sort({ _id : -1 }).toArray((err, docs) => {
            cb(err, docs);
        });
    }

    static getCommentsByBlogId(_blogId, cb) {
        db.get().collection('comments').find({blogId: ObjectID(_blogId), isDeleted: false}).toArray((err, docs) => {
            cb(err, docs);
        });
    }

    static deleteCommentById(_commentId, cb) {
        db.get().collection('comments').deleteOne({_id: ObjectID(_commentId)},(err, result) => {
                cb(err, result);
            });
    }
}

module.exports = CommentModel;
