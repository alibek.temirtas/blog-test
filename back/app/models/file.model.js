const ObjectID = require('mongodb').ObjectID;
const db = require('../db');

const DbEntity = require('./dbentity.model');

class FileModel extends DbEntity {
    constructor(obj) {
        super();
        this.name = obj.name;
        this.type = obj.type;
        this.size = obj.size;
        this.path = obj.path;
    }

    create(cb) {
        db.get().collection('files').insertOne(this, (err, result) => {
            cb(err, result);
        });
    }

    static getFiles(data, cb) {
        db.get().collection('files').find().sort({ _id : -1 }).limit(35).toArray((err, docs) => {
            cb(err, docs);
        });
    }
}

module.exports = FileModel;
