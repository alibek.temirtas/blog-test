const moment = require('moment');

class DbEntityModel {
    constructor(_createdDate,
                _updatedDate,
                _deletedDate,
                _isDeleted) {
        this.createdDate = moment.now();
        this.updatedDate = _updatedDate || null;
        this.deletedDate = _deletedDate || null;
        this.isDeleted = _isDeleted || false;
    }
}

module.exports = DbEntityModel;
