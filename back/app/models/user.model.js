const ObjectID = require('mongodb').ObjectID;
const db = require('../db');
const moment = require('moment');

const DbEntity = require('./dbentity.model');

class UserModel extends DbEntity {
    constructor(obj) {
        super();
        this.phoneNumber = obj.phoneNumber;
        this.password = obj.password;
        this.role = obj.role;
        this.profile = obj.profile;
    }

    create(cb) {
        db.get().collection('users').insertOne(this, (err, result) => {
            cb(err, result);
        });
    }

    static getUsers(query, cb) {
        db.get().collection('users').find().skip((query.perPage * query.page) - query.perPage).limit(query.perPage).sort({ _id : -1 }).toArray((err, docs) => {
            cb(err, docs);
        });
    }

    static getCount() {
        return db.get().collection('users').find().count();
    }

    static findById(_userId, cb) {
        db.get().collection('users').findOne({_id: ObjectID(_userId)}, (err, result) => {
            cb(err, result);
        });
    }

    static isExist(_phoneNumber, cb) {
        db.get().collection('users').findOne({
            phoneNumber: _phoneNumber
        }, (err, result) => {
            cb(err, result);
        });
    }

    static findByIdAndUpdate(_id, formdata, cb) {
        db.get().collection('users').updateOne({_id: ObjectID(_id)},
            {
                $set: {
                    ...formdata,
                    updatedDate: moment.now()
                }
            },
            (err, result) => {
                cb(err, result);
            });
    }

    static deleteUserById(_userId, formdata, cb) {
        db.get().collection('users').updateOne({_id: ObjectID(_userId)},
            {
                $set: {
                    ...formdata,
                    deletedDate: moment.now()
                }
            },
            (err, result) => {
                cb(err, result);
            });
    }

}

module.exports = UserModel;
