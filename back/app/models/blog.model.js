const ObjectID = require('mongodb').ObjectID;
const db = require('../db');
const moment = require('moment');

const status = require('../_constants/blog_status');
const DbEntity = require('./dbentity.model');

class BlogModel extends DbEntity {
    constructor(obj) {
        super();
        this.userId = ObjectID(obj.userId);
        this.author = {
          id: ObjectID(obj.userId),
          ...obj.profile
        };
        this.title = obj.title;
        this.desc = obj.desc;
        this.status = status.MODERATION;
        this.filePath = obj.filePath || null;
        this.visits = [];
        this.likes = [];
    }

    create(cb) {
        db.get().collection('blogs').insertOne(this, (err, result) => {
            cb(err, result);
        });
    }

    static getAllBlogs(query, cb) {
        db.get().collection('blogs').find({status: status.APPROVED, isDeleted: false}).skip((query.perPage * query.page) - query.perPage).limit(query.perPage).sort({ _id : -1}).toArray((err, docs) => {
            cb(err, docs);
        });
    }

    static getAllBlogsForAdmin(query, cb) {
        db.get().collection('blogs').find().skip((query.perPage * query.page) - query.perPage).limit(query.perPage).sort({ _id : -1}).toArray((err, docs) => {
            cb(err, docs);
        });
    }

    static getBlogsByUserId(_userId, query, cb) {
        db.get().collection('blogs').find({userId: ObjectID(_userId), isDeleted: false}).skip((query.perPage * query.page) - query.perPage).limit(query.perPage).sort({ _id : -1 }).toArray((err, docs) => {
            cb(err, docs);
        });
    }

    static getCount(query) {
        return db.get().collection('blogs').find(query).count();
    }

    static findById(_blogId, cb) {
        db.get().collection('blogs').findOne({_id: ObjectID(_blogId)}, (err, doc) => {
            cb(err, doc);
        });
    }

    static findByIdAndUpdate(_id, formdata, cb) {
        db.get().collection('blogs').updateOne({_id: ObjectID(_id)},
            {
                $set: {
                    ...formdata,
                    updatedDate: moment.now()
                }
            },
            (err, result) => {
                cb(err, result);
            });
    }

    static addVisiter(_blogId, optData, cb) {
        db.get().collection('blogs').updateOne(
            {_id: ObjectID(_blogId)},
            {
                $push: {
                    visits: {
                        ...optData
                    }
                }
            },
            (err, result) => {
                cb(err, result);
            });
    }

    static addLike(_blogId, optData, cb) {
        db.get().collection('blogs').updateOne(
            {_id: ObjectID(_blogId)},
            {
                $push: {
                    likes: {
                        ...optData
                    }
                }
            },
            (err, result) => {
                cb(err, result);
            });
    }

    static deleteBlogById(_blogId, formdata, cb) {
        db.get().collection('blogs').updateOne({_id: ObjectID(_blogId)},
            {
                $set: {
                    ...formdata,
                    deletedDate: moment.now()
                }
            },
            (err, result) => {
                cb(err, result);
            });
    }

}

module.exports = BlogModel;
