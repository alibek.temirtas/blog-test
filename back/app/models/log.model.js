const ObjectID = require('mongodb').ObjectID;
const db = require('../db');

const DbEntity = require('./dbentity.model');

class LogModel extends DbEntity {
    constructor(obj) {
        super();
        this.userId = ObjectID(obj.userId);
        this.eventType = obj.eventType;
        this.IPAdress = obj.IPAdress;
        this.desc = obj.desc;
    }

    create(cb) {
        db.get().collection('logs').insertOne(this, (err, result) => {
            console.log('logging .... ', this);
            cb(err, result);
        });
    }

    static getLogsByUserId(data, cb) {
        db.get().collection('logs').find({userId: ObjectID(data.userId)}).sort({ _id : -1 }).limit(35).toArray((err, docs) => {
            cb(err, docs);
        });
    }
}

module.exports = LogModel;
