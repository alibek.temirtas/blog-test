const http = require('http');

const app = require('./app/app');
const db = require('./app/db');
const config = require('./app/_constants/config');

const PORT_HTTP = config.PORT_HTTP;
const URL = config.SERVER_URL;

const httpServer = http.createServer(app);

db.connect('mongodb://' + URL + ':27017/blog', (err) => {
    if (err) {
        console.log('Error db connection: ', err);
        return;
    }

    httpServer.listen(PORT_HTTP, () => {
        console.log("http server running at http://" + URL + ":" + PORT_HTTP + '/');
    });

});
