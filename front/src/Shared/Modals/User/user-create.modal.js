import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Alert, Input, Modal, Select} from "antd";

import {userActions} from "../../../_actions";

const Option = Select.Option;

class UserCreateModalComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            newUser: {
                phoneNumber: null,
                password: null,
                name: null,
                surname: null
            },
            submittedCreate: false
        };

    }

    componentWillMount() {
    }


    handleChange = (event) => {
        const {name, value} = event.target;
        const {newUser} = this.state;
        this.setState({
            newUser: {
                ...newUser,
                [name]: value
            }
        });
    };

    handleChangeRole = (value) => {
        const {newUser} = this.state;
        this.setState({
            newUser: {
                ...newUser,
                role: value
            }
        });
    };

    handleOk = (e) => {
        const {newUser} = this.state;
        const {onOk} = this.props;

        this.setState({
            newUser: {
                phoneNumber: null,
                password: null,
                name: null,
                surname: null
            },
            submittedCreate: true
        });

        if (newUser.name && newUser.surname) {
            this.props.dispatch(userActions.createUser(newUser));
            this.setState({
                submittedCreate: false
            });
            onOk();
        }
    };

    handleCancel = (e) => {
        const {onCancel} = this.props;
        this.setState({
            newUser: {
                phoneNumber: null,
                password: null,
                name: null,
                surname: null
            }
        });
        onCancel();
    };

    render() {
        const {visible} = this.props;
        const {submittedCreate, newUser} = this.state;

        return (
            <React.Fragment>
                <Modal title="Создание пользователя"
                       visible={visible}
                       onOk={this.handleOk}
                       onCancel={this.handleCancel}>
                    <form className="form" name="form">
                        <Input className="form__item" placeholder="Введите номер телефона" type="text"
                               name="phoneNumber"
                               value={newUser.phoneNumber} onChange={this.handleChange}/>
                        {submittedCreate && !newUser.phoneNumber &&
                        <Alert message="Номер телефона обязательно" type="error"/>}

                        <Input className="form__item" placeholder="Введите имя" type="text" name="name"
                               value={newUser.name} onChange={this.handleChange}/>
                        {submittedCreate && !newUser.name && <Alert message="Имя обязательно" type="error"/>}

                        <Input className="form__item" placeholder="Введите фамилию" type="text" name="surname"
                               value={newUser.surname} onChange={this.handleChange}/>
                        {submittedCreate && !newUser.surname && <Alert message="Фамилия обязательно" type="error"/>}

                        <Input className="form__item" placeholder="Введите пароль" type="text" name="password"
                               value={newUser.password} onChange={this.handleChange}/>
                        {submittedCreate && !newUser.password && <Alert message="пароль обязательно" type="error"/>}

                        <Select defaultValue="1" name="role" onChange={this.handleChangeRole}>
                            <Option value="1">User</Option>
                            <Option value="2">Admin</Option>
                        </Select>
                    </form>
                </Modal>
            </React.Fragment>
        );
    }

}

function mapStateToProps(state) {
    return {
        state
    };
}

const connectedUserCreateModalComponent = connect(mapStateToProps)(UserCreateModalComponent);
export {connectedUserCreateModalComponent as UserCreateModalComponent};
