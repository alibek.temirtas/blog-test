import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Alert, Input, Modal} from "antd";
import {userActions} from "../../../_actions";

class UserEditModalComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editUser: null,
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            editUser: nextProps.user
        });
    }

    componentWillMount() {
    }

    handleChange(event) {
        const {name, value} = event.target;
        const {editUser} = this.state;
        this.setState({
            editUser: {
                ...editUser,
                profile: {
                    ...editUser.profile,
                    [name]: value
                }
            }
        });
    };

    handleOk = (e) => {
        const {onOk} = this.props;
        const {editUser} = this.state;

        this.setState({
            submitted: true
        });

        if (editUser.profile.name && editUser.profile.surname) {
            this.props.dispatch(userActions.updateUser(editUser));
            this.setState({
                submitted: false
            });
            onOk();
        }
    };

    handleCancel = (e) => {
        const {onCancel} = this.props;
        onCancel();
    };

    render() {
        const {visible, user} = this.props;
        let {editUser, submitted} = this.state;

        return (
            <React.Fragment>
                {user && editUser &&
                <Modal title="Редактирование пользователя"
                       visible={visible}
                       onOk={this.handleOk}
                       onCancel={this.handleCancel}>
                    <form className="form" name="form">

                        <Input className="form__item" placeholder="Введите имя" type="text" name="name"
                               value={editUser.profile.name} onChange={this.handleChange}/>
                        {submitted && !editUser.profile.name && <Alert message="Имя обязательно" type="error"/>}

                        <Input className="form__item" placeholder="Введите фамилию" type="text" name="surname"
                               value={editUser.profile.surname} onChange={this.handleChange}/>
                        {submitted && !editUser.profile.surname && <Alert message="Фамилия обязательно" type="error"/>}

                    </form>
                </Modal>
                }
            </React.Fragment>
        );
    }

}

function mapStateToProps(state) {
    const {users} = state;
    return {
        users
    };
}

const connectedUserEditModalComponent = connect(mapStateToProps)(UserEditModalComponent);
export {connectedUserEditModalComponent as UserEditModalComponent};
