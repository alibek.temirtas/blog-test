import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Alert, Input, Modal} from "antd";
import TextArea from "antd/lib/input/TextArea";

import {blogActions} from "../../../_actions/blog.actions";

class BlogEditModalComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editBlog: null,
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            editBlog: nextProps.blog
        });
    }

    componentWillMount() {
    }

    handleChange(event) {
        const {name, value} = event.target;
        const {editBlog} = this.state;
        this.setState({
            editBlog: {
                ...editBlog,
                [name]: value
            }
        });
    };

    handleOk = (e) => {
        const {handleOk} = this.props;
        const {editBlog} = this.state;

        this.setState({
            submitted: true
        });

        if (editBlog.title && editBlog.desc) {
            this.props.dispatch(blogActions.updateBlog(this.state.editBlog));
            this.setState({
                submitted: false
            });
            handleOk();
        }
    };

    handleCancel = (e) => {
        const {handleCancel} = this.props;
        handleCancel();
    };

    render() {
        const {visible, blog} = this.props;
        let {editBlog, submitted} = this.state;

        return (
            <React.Fragment>
                {blog && editBlog &&
                <Modal title="Редактирование блога"
                       visible={visible}
                       onOk={this.handleOk}
                       onCancel={this.handleCancel}>
                    <form className="form" name="form">

                        <Input className="form__item" placeholder="Введите заголовок" type="text" name="title"
                               value={editBlog.title} onChange={this.handleChange}/>
                        {submitted && !editBlog.title && <Alert message="Заголовок обязательно" type="error"/>}

                        <TextArea className="form__item" placeholder="Введите описание" type="text" name="desc"
                                  value={editBlog.desc} onChange={this.handleChange}/>
                        {submitted && !editBlog.desc && <Alert message="Описание обязательно" type="error"/>}

                    </form>
                </Modal>
                }
            </React.Fragment>
        );
    }

}

function mapStateToProps(state) {
    const {blogs} = state;
    return {
        blogs
    };
}

const connectedBlogEditModalComponent = connect(mapStateToProps)(BlogEditModalComponent);
export {connectedBlogEditModalComponent as BlogEditModalComponent};
