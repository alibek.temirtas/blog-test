import React, {Component} from 'react';
import {connect} from 'react-redux';

import {history} from "../../../_helpers";
import {alertActions} from "../../../_actions/alert.actions";
import {Modal} from "antd";

class AlertModalComponent extends Component {
    constructor(props) {
        super(props);
        const {dispatch} = this.props;
        history.listen((location, action) => {
            dispatch(alertActions.clear());
        });
    }

    componentWillMount() {
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const alert = this.props;
        let _alert = alert.alert;
        {
            _alert && _alert.type === 'error' ?
                this.error(_alert.message) :
                _alert.type === 'success' ?
                    this.success(_alert.message) :
                    console.log('alert nothing...');
        }
    }

    success(msg) {
        Modal.success({
            title: msg,
            content: '',
        });
    }

    error(msg) {
        Modal.error({
            title: msg,
            content: '',
        });
        // alert.alert.message
    }

    render() {

        return (
            <div>
            </div>
        );
    }

}

function mapStateToProps(state) {
    const {alert} = state;
    return {
        alert
    };
}

const connectedAlertModalComponent = connect(mapStateToProps)(AlertModalComponent);
export {connectedAlertModalComponent as AlertModalComponent};
