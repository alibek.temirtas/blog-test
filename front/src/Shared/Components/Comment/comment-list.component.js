import React, {Component} from 'react';
import {Comment, Avatar, Tooltip, Button, Icon} from "antd";
import moment from 'moment';

import io from 'socket.io-client';

import {CommentCreateComponent} from './comment-create.component';
import {connect} from "react-redux";
import {commentActions} from "../../../_actions/comment.actions";

class CommentListComponent extends Component {

    componentWillMount() {
        const {blog, dispatch} = this.props;
        const socket = io.connect('http://localhost:4300');

        dispatch(commentActions.getCommentsByBlogId(blog._id));

        socket.emit('blog', {
            blogId: blog._id
        });

        socket.on('comments', (data)=>{
            console.log('socket: ', data);
            dispatch(commentActions.pushComment(data));
        });

        socket.on('deleteComment', (id)=>{
            console.log('socket: ', id);
            dispatch(commentActions.popComment(id));
        });
    }

    handleDeleteComment = (comment) => {
        this.props.dispatch(commentActions.deleteComment(comment));
    };

    render() {

        const {comments, user} = this.props;
        return (
            <React.Fragment>
                {comments.items && comments.items.map((item) => {
                    return (
                        <div className="outline" key={item._id}>
                            <Comment
                                actions={[]}
                                author={<span>{item.author.name} {item.author.surname}</span>}
                                avatar={(
                                    <Avatar
                                        src='/assets/img/avatar.jpg'
                                        alt={item.author}
                                    />
                                )}
                                content={(
                                    <div>
                                        <p>{item.text}</p>
                                    </div>
                                )}
                                datetime={(
                                    <div>
                                        <Tooltip className="margin-right" title={moment(item.createdDate).format('YYYY-MM-DD HH:mm:ss')}>
                                            <span>{moment(item.createdDate).fromNow()}</span>
                                        </Tooltip>
                                        {user.user._id === item.userId &&
                                            <Button type="danger" onClick={()=>{this.handleDeleteComment(item)}} className="float-right">
                                                <Icon type="close"></Icon>
                                            </Button>
                                        }

                                    </div>
                                )}
                            />
                        </div>
                    )
                })
                }
                <CommentCreateComponent/>
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    const {authentication, comments, blogs} = state;
    const {user} = authentication;
    const {blog} = blogs;
    return {
        user,
        comments,
        blog
    };
}

const connectedCommentCommentListComponent = connect(mapStateToProps)(CommentListComponent);
export {connectedCommentCommentListComponent as CommentListComponent};
