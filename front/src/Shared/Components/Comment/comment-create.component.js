import React, {Component} from 'react';
import {Alert, Button, Input} from "antd";
import {commentActions} from "../../../_actions/comment.actions";
import {connect} from "react-redux";

class CommentCreateComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            comment: {
                text: null
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.sendComment.bind(this);
    }

    handleChange(event) {
        const {name, value} = event.target;
        const {comment} = this.state;
        this.setState({
            comment: {
                ...comment,
                [name]: value
            }
        });
    }

    sendComment(e) {
        e.preventDefault();

        this.setState({submitted: true});
        const {comment} = this.state;
        const {dispatch, blog} = this.props;
        if (comment.text) {
            comment.blogId = blog._id;
            dispatch(commentActions.createComment(comment));
            this.setState({
                comment: {
                    text: null
                },
                submitted: false
            });
        }
    }

    render() {
        const {comment, submitted} = this.state;
        return (
            <React.Fragment>
                <div>
                    <div className="form">

                        <Input className="form__item" placeholder="Введите текст" type="text" name="text"
                               value={comment.text} onChange={this.handleChange}/>
                        {submitted && !comment.text && <Alert message="Текст обязательно" type="error"/>}

                        <Button type="primary" onClick={this.handleSubmit}>Отправить комментарий</Button>

                    </div>
                </div>
            </React.Fragment>
        )
    }
}
function mapStateToProps(state) {
    const {blog} = state.blogs;
    return {
        blog
    };
}

const connectedCommentCreateComponent = connect(mapStateToProps)(CommentCreateComponent);
export {connectedCommentCreateComponent as CommentCreateComponent};
