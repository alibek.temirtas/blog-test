import React, {Component} from 'react';
import {Card, Button, Icon, Empty, Pagination} from "antd";
import moment from 'moment';
import {Link} from "react-router-dom";

class BlogList extends Component {

    render() {
        let {blogs, pages, extended, status, adminPanel, defaultPageNumber} = this.props;
        const {deleteBlog, editBlog, recoverBlog, approveBlog, rejectBlog, onChangePage} = this.props;

        /*
            Здесь по-хорошему лучше делать запрос на сервак и через редакс. Не успел)
         */
        if (status && blogs) {
            blogs = blogs.filter((item) => {
                return item.status === +status;
            });
        }


        return (
            <div>
                {blogs && blogs.map((item) => {
                    return (
                        <div className="blog_panel" key={item._id}>
                            <Card className="blog_item" title={item.title} extra={<Link to={'/detail/' + item._id}><Button icon="info-circle"type="default"> Подробнее</Button></Link>}>
                                <div className="blog_item__wr">
                                    <div className="img-wr">
                                        <img src="/assets/img/avatar.jpg" alt="" width={70}/>
                                    </div>
                                    <h2>{item.author.name} {item.author.surname}</h2>

                                    {adminPanel &&
                                        <p>{item.status === 200? 'Принято' : item.status === 100? 'На модерации' : 'Отказано' }</p>
                                    }

                                </div>
                                <p>{moment(item.createdDate).format('DD/MM/YYYY HH:mm')}</p>
                                <p><span><Icon type="eye"/> {item.visits}</span></p>

                                <p>
                                    {extended && !adminPanel && <Button className="margin-right" type="danger" icon="close" onClick={() => {deleteBlog(item._id)}}>Удалить</Button>}
                                    {extended && <Button type="default" icon="edit" onClick={() => {editBlog(item)}}>Редактировать</Button>}
                                </p>

                                <p>
                                    {adminPanel && !item.isDeleted && <Button className="margin-right" type="danger" icon="close" onClick={() => {deleteBlog(item)}}>Удалить</Button>}
                                    {adminPanel && item.isDeleted && <Button type="default" icon="reload" onClick={() => {recoverBlog(item)}}>Восстановить</Button>}
                                </p>
                                <p>
                                    {adminPanel && <Button className="btn-success margin-right" type="primary" icon="check" onClick={() => {approveBlog(item)}}>Принять</Button>}
                                    {adminPanel && <Button type="danger" icon="close" onClick={() => {rejectBlog(item)}}>Отказать</Button>}
                                </p>
                            </Card>
                        </div>
                    )
                })}
                {blogs && !(blogs.length > 0) &&
                <Empty
                    image="/assets/img/original.png"
                    description={
                        <span>
                            К сожалению, ничего нет, жи есть
                          </span>}>

                    {+status === 200 ?
                        'Ваш блог на модерации' :
                        +status === 100 ?
                            <Link to="/account/create" className="btn btn-link"><Button type="primary"><Icon
                                type="plus"/> Создать блог</Button></Link> : ''
                    }

                </Empty>
                }

                {pages && (blogs.length > 0) && <Pagination className="margin-top margin-bottom" defaultCurrent={defaultPageNumber} total={pages*10} onChange={onChangePage} />}
            </div>
        )
    }
}

export default BlogList;
