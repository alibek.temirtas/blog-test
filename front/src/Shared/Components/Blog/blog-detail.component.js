import React, {Component} from 'react';
import {Card, Icon} from "antd";
import moment from 'moment';

import {CommentListComponent} from '../Comment/comment-list.component';
import {serverConstants} from "../../../_constants";

// Глупый компонент
class BlogDetailComponent extends Component {

    render() {
        const {blog} = this.props;

        return (
            <React.Fragment>
                {blog && <div className="outline">
                    <div className="blog_panel" key={blog._id}>
                        <Card className="blog_item" title={blog.title}>
                            <div className="blog_item__wr">
                                <div className="img-wr">
                                    <img src="/assets/img/avatar.jpg" alt="" width={70}/>
                                </div>
                                <h2>{blog.author.name} {blog.author.surname}</h2>
                                <p>{moment(blog.createdDate).format('DD/MM/YYYY HH:mm')}</p>
                                <p><span><Icon type="eye" /> {blog.visits}</span></p>
                            </div>
                            <div className="clearfix"></div>
                            <hr/>
                            {blog.filePath &&
                            <div style={{textAlign: 'center'}}>
                                <img width="400px" src={serverConstants.SERVER_URL + blog.filePath} alt=""/>
                            </div>
                            }
                            <p>{blog.desc}</p>
                            <CommentListComponent/>
                        </Card>
                    </div>
                </div>
                }
            </React.Fragment>
        )
    }
}

export default BlogDetailComponent;
