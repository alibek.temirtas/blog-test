import React, {Component} from 'react';
import {Card, Button, Icon, Empty, Pagination} from "antd";
import moment from 'moment';

class UserList extends Component {

    render() {
        let {users, defaultPageNumber} = this.props;
        const {deleteUser, editUser, recoverUser, onChangePage} = this.props;


        return (
            <div>
                {users.items && users.items.map(item => {
                    return (
                        <div key={item._id}>
                            <Card className="blog_item" title={item.profile.name + " " + item.profile.surname}>
                                <p>{moment(item.createdDate).format('DD/MM/YYYY HH:mm')}</p>

                                <p>
                                    {item.isDeleted ?
                                        <Button className="btn-success margin-right"
                                                type="primary" onClick={()=>{recoverUser(item)}}><Icon type="reload"></Icon> Восстановить</Button>:
                                        <Button className="margin-right" type="danger" onClick={()=>{deleteUser(item)}}><Icon type="close"></Icon> Удалить</Button>
                                    }
                                    <Button type="primary" onClick={()=>{editUser(item)}}><Icon type="edit"></Icon> Редактировать</Button>
                                </p>
                            </Card>
                        </div>
                    )
                })}
                {users.items && !(users.items.length > 0) &&
                <Empty
                    image="/assets/img/original.png"
                    description={
                        <span>
                            К сожалению, ничего нет, жи есть
                          </span>}>

                </Empty>
                }

                {users.pages && <Pagination className="margin-top margin-bottom" defaultCurrent={defaultPageNumber} total={users.pages*10} onChange={onChangePage} />}
            </div>
        )
    }
}

export default UserList;
