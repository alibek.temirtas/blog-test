import React, {Component} from 'react';

import {serverConstants} from "../../../_constants";
import {connect} from "react-redux";
import {Button, Icon} from "antd";

import {fileUploadActions} from "../../../_actions/file-upload.actions";
import {history} from "../../../_helpers";

// Глупый компонент
class UploadFileComponent extends Component {
    constructor(props) {
        super(props);
        const {dispatch} = this.props;
        history.listen((location, action) => {
            dispatch(fileUploadActions.clear());
        });
    }

    clearFile = () => {
        const {dispatch} = this.props;
        dispatch(fileUploadActions.clear());
    }

    selectFile() {
        let file = document.getElementById('file');
        file.click();
    }

    handleFileUpload = (e) => {
        const file = e.target.files.item(0);

        const { dispatch } = this.props;
        dispatch(fileUploadActions.uploadFile(file));
    };

    render() {
        const {files} = this.props;

        return (
            <React.Fragment>

                <input id="file" className="hidden" type="file" onChange={this.handleFileUpload} />
                <Button type="primary" className="margin-bottom" onClick={this.selectFile}>
                    <Icon type="upload"></Icon> Добавить файл
                </Button>

                {files.file &&
                <div className="file-upload_wr">
                    <img src={serverConstants.SERVER_URL + files.file.path} alt=""/>
                    <Button type="danger" onClick={this.clearFile}><Icon type="close"></Icon></Button>
                </div>
                }
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    const {files} = state;
    return {
        files
    };
}

const connectedUploadFileComponent = connect(mapStateToProps)(UploadFileComponent);
export {connectedUploadFileComponent as UploadFileComponent};
