import {userConstants} from '../_constants';

import {userService} from "../_services/user.service";
import {history} from "../_helpers";

import {alertActions} from "./alert.actions";

export const userActions = {
    login,
    logout,
    getAll,
    updateUser,
    createUser,
    delete: _delete
};

function login(phoneNumber, password) {
    return dispatch => {

        userService.login(phoneNumber, password)
            .then(
                user => {
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error));

                    // Здесь я ошибки передаю. Но к сожалению, не успел их обработать на фронте.
                    dispatch(alertActions.error('Не верный логин или пароль'));
                }
            );
    };

    function success(user) {
        return {type: userConstants.LOGIN_SUCCESS, user}
    }

    function failure(error) {
        return {type: userConstants.LOGIN_FAILURE, error}
    }
}

function getAll(page) {
    return dispatch => {

        userService.getAll(page)
            .then(
                data => {
                    dispatch(success(data));
                },
                error => {
                    dispatch(failure(error));
                }
            );
    };

    function success(data) {
        return {type: userConstants.GETALL_SUCCESS, data}
    }

    function failure(error) {
        return {type: userConstants.GETALL_FAILURE, error}
    }
}

function updateUser(user) {
    return dispatch => {

        userService.update(user)
            .then(
                data => {
                    dispatch(success(data));
                    dispatch(alertActions.success('Успешно'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error('Хм..'));
                }
            );
    };

    function success(user) {
        return {type: userConstants.UPDATE_SUCCESS, user}
    }

    function failure(error) {
        return {type: userConstants.UPDATE_FAILURE, error}
    }
}

function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        userService.delete(id)
            .then(
                user => {
                    dispatch(success(id));
                    dispatch(alertActions.success('Пользователь успешно удален'));
                },
                error => {
                    dispatch(failure(id, error));
                    dispatch(alertActions.error('Хм..'));
                }
            );
    };

    function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
}

function createUser(user) {
    return dispatch => {

        userService.createUserByAdmin(user)
            .then(
                data => {
                    dispatch(success(data));
                    dispatch(alertActions.success('Пользователь успешно создан'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error('Хм..'));
                }
            );
    };

    function success(user) {
        return {type: userConstants.CREATE_USER_SUCCESS, user}
    }

    function failure(error) {
        return {type: userConstants.CREATE_USER_FAILURE, error}
    }
}

function logout() {
    userService.logout();
    return {type: userConstants.LOGOUT};
}
