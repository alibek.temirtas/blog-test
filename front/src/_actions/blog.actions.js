import {blogConstants} from "../_constants/blog.constants";

import {blogService} from "../_services/blog.service";
import {history} from "../_helpers";

import {alertActions} from "./alert.actions";

export const blogActions = {
    create,
    getAll,
    getBlogsByUserId,
    getBlogById,
    updateBlog,
    getAllBlogForAdmin,
    delete: _delete
};


function getAll(page) {
    return dispatch => {
        dispatch(request());

        blogService.getAll(page)
            .then(
                data => dispatch(success(data)),
                error => dispatch(failure(error))
            );
    };

    function request() {
        return {type: blogConstants.GETALL_REQUEST}
    }

    function success(data) {
        return {type: blogConstants.GETALL_SUCCESS, data}
    }

    function failure(error) {
        return {type: blogConstants.GETALL_FAILURE, error}
    }
}

function getAllBlogForAdmin(page) {
    return dispatch => {
        dispatch(request());

        blogService.getAllBlogsForAdmin(page)
            .then(
                data => dispatch(success(data)),
                error => dispatch(failure(error))
            );
    };

    function request() {
        return {type: blogConstants.GETALL_REQUEST}
    }

    function success(data) {
        return {type: blogConstants.GETALL_SUCCESS, data}
    }

    function failure(error) {
        return {type: blogConstants.GETALL_FAILURE, error}
    }
}

function getBlogsByUserId(page) {
    return dispatch => {

        blogService.getBlogsByUserId(page)
            .then(
                data => dispatch(success(data)),
                error => dispatch(failure(error))
            );
    };

    function success(data) {
        return {type: blogConstants.GETALL_SUCCESS, data}
    }

    function failure(error) {
        return {type: blogConstants.GETALL_FAILURE, error}
    }
}

function create(blog) {
    return dispatch => {

        blogService.createBlog(blog)
            .then(
                data => {
                    dispatch(success(data));
                    history.push('/account/my?status=100');
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error('Ошибка при создании'));
                }
            );
    };

    function success(blog) {
        return {type: blogConstants.CREATE_SUCCESS, blog}
    }

    function failure(error) {
        return {type: blogConstants.CREATE_FAILURE, error}
    }
}

function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        blogService.deleteBlog(id)
            .then(
                blog => {
                    dispatch(success(id));
                    dispatch(alertActions.success('Блог успешно удален'));
                },
                error => {
                    dispatch(failure(id, error));
                    dispatch(alertActions.error('Хм..'));
                }
            );
    };

    function request(id) {
        return {type: blogConstants.DELETE_REQUEST, id}
    }

    function success(id) {
        return {type: blogConstants.DELETE_SUCCESS, id}
    }

    function failure(id, error) {
        return {type: blogConstants.DELETE_FAILURE, id, error}
    }
}

function updateBlog(blog) {
    return dispatch => {
        dispatch(request(blog));

        blogService.updateBlog(blog)
            .then(
                data => {
                    dispatch(success(data));
                    dispatch(alertActions.success('Блог успешно обновлен'));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error('Ошибочка..'));
                }
            );
    };

    function request(blog) {
        return {type: blogConstants.UPDATE_REQUEST, blog}
    }

    function success(blog) {
        return {type: blogConstants.UPDATE_SUCCESS, blog}
    }

    function failure(error) {
        return {type: blogConstants.UPDATE_FAILURE, error}
    }
}

function getBlogById(id) {
    return dispatch => {

        blogService.getBlogById(id)
            .then(
                blog => {
                    dispatch(success(blog));
                },
                error => {
                    dispatch(failure(id, error));
                }
            );
    };

    function success(blog) {
        return {type: blogConstants.GET_BLOGBYID_SUCCESS, blog}
    }

    function failure(id, error) {
        return {type: blogConstants.GET_BLOGBYID_FAILURE, id, error}
    }
}
