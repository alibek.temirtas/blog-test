import {fileUploadConstants} from "../_constants";

import {fileUploadService} from "../_services/file-upload.service";

export const fileUploadActions = {
    uploadFile,
    clear
};

function uploadFile(file) {
    return dispatch => {

        fileUploadService.uploadFile(file)
            .then(
                data => dispatch(success(data)),
                error => dispatch(failure(error))
            );
    };

    function success(data) {
        return {type: fileUploadConstants.CREATE_SUCCESS, data}
    }

    function failure(error) {
        return {type: fileUploadConstants.CREATE_FAILURE, error}
    }
}

function clear() {
    return { type: fileUploadConstants.CLEAR };
}
