import {commentService} from "../_services/comment.service";

import {commentConstants} from "../_constants";

export const commentActions = {
    createComment,
    getCommentsByBlogId,
    deleteComment,
    pushComment,
    popComment
};

function createComment(comment) {
    return dispatch => {

        commentService.createComment(comment)
            .then(
                data => {
                    // dispatch(success(data));
                },
                error => {
                    dispatch(failure(error));
                }
            );
    };

    function success(comment) {
        return {type: commentConstants.CREATE_SUCCESS, comment}
    }

    function failure(error) {
        return {type: commentConstants.CREATE_FAILURE, error}
    }
}

function pushComment(comment) {
    return {type: commentConstants.PUSH_COMMENT, comment}
}

function popComment(id) {
    return {type: commentConstants.POP_COMMENT, id}
}

function getCommentsByBlogId(blogId) {
    return dispatch => {

        commentService.getCommentsByBlogId(blogId)
            .then(
                comments => dispatch(success(comments)),
                error => dispatch(failure(error))
            );
    };

    function success(comments) {
        return {type: commentConstants.GETALL_BYBLOGID_SUCCESS, comments}
    }

    function failure(error) {
        return {type: commentConstants.GETALL_BYBLOGID_FAILURE, error}
    }
}

function deleteComment(comment) {
    return dispatch => {
        dispatch(request(comment._id));

        commentService.deleteComment(comment)
            .then(
                data => {
                    // dispatch(success(comment._id));
                },
                error => {
                    dispatch(failure(comment._id, error));
                }
            );
    };

    function request(id) {
        return {type: commentConstants.DELETE_REQUEST, id}
    }

    function success(id) {
        return {type: commentConstants.DELETE_SUCCESS, id}
    }

    function failure(id, error) {
        return {type: commentConstants.DELETE_FAILURE, id, error}
    }
}
