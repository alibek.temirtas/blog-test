import {serverConstants} from "../_constants";

import {authHeader} from "../_helpers";

export const commentService = {
    createComment,
    getCommentsByBlogId,
    deleteComment
};

function createComment(comment) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(comment)
    };

    return fetch(`${serverConstants.SERVER_URL}/comment`, requestOptions).then(handleResponse);
}

function getCommentsByBlogId(blogId) {
    const requestOptions = {
        method: 'GET',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify()
    };

    return fetch(`${serverConstants.SERVER_URL}/comment/${blogId}`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}


function deleteComment(comment) {
    const requestOptions = {
        method: 'DELETE',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({comment: comment})
    };

    return fetch(`${serverConstants.SERVER_URL}/comment`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            return Promise.reject(data);
        }

        return data;
    });
};
