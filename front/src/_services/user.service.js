import {serverConstants} from "../_constants";
import {authHeader} from "../_helpers";

export const userService = {
    login,
    logout,
    register,
    activate,
    getAll,
    update,
    delete: _delete,
    createUserByAdmin
};

function login(phoneNumber, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ phoneNumber, password })
    };

    return fetch(`${serverConstants.SERVER_URL}/login`, requestOptions)
        .then(handleResponse)
        .then(user => {
            localStorage.setItem('user', JSON.stringify(user));
            return user;
        });
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(user)
    };

    return fetch(`${serverConstants.SERVER_URL}/register`, requestOptions).then(handleResponse);
}

function activate(obj) {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(obj)
    };

    return fetch(`${serverConstants.SERVER_URL}/activate`, requestOptions).then(handleResponse);
}

function getAll(page) {
    const requestOptions = {
        method: 'GET',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify()
    };

    return fetch(`${serverConstants.SERVER_URL}/user/all${page? '?page='+page: ''}`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}

function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${serverConstants.SERVER_URL}/user`, requestOptions).then(handleResponse);
}

function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({
            userId: id
        })
    };

    return fetch(`${serverConstants.SERVER_URL}/user`, requestOptions).then(handleResponse);
}

function createUserByAdmin(user) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${serverConstants.SERVER_URL}/user`, requestOptions).then(handleResponse);
}

function logout() {
    localStorage.removeItem('user');
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            return Promise.reject(data);
        }

        return data;
    });
}
