import {serverConstants} from "../_constants";

import {authHeader} from "../_helpers";

export const blogService = {
    getAll,
    createBlog,
    getBlogsByUserId,
    getBlogById,
    deleteBlog,
    updateBlog,
    getAllBlogsForAdmin
};

function getAll(page) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify()
    };

    return fetch(`${serverConstants.SERVER_URL}/blog${page? '?page='+page: ''}`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}

function getAllBlogsForAdmin(page) {
    const requestOptions = {
        method: 'GET',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify()
    };

    return fetch(`${serverConstants.SERVER_URL}/blog/admin${page? '?page='+page: ''}`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}

function getBlogsByUserId(page) {
    const requestOptions = {
        method: 'GET',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify()
    };

    return fetch(`${serverConstants.SERVER_URL}/blog/byuser${page? '?page='+page: ''}`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}

function createBlog(blog) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(blog)
    };

    return fetch(`${serverConstants.SERVER_URL}/blog`, requestOptions).then(handleResponse);
}

function updateBlog(blog) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(blog)
    };

    return fetch(`${serverConstants.SERVER_URL}/blog`, requestOptions).then(handleResponse);
}

function deleteBlog(blogId) {
    const requestOptions = {
        method: 'DELETE',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({blogId: blogId})
    };

    return fetch(`${serverConstants.SERVER_URL}/blog`, requestOptions).then(handleResponse);
}

function getBlogById(id) {
    const requestOptions = {
        method: 'GET',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify()
    };

    return fetch(`${serverConstants.SERVER_URL}/blog/detail/${id}`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            return Promise.reject(data);
        }

        return data;
    });
};
