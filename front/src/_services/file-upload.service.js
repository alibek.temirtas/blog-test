import {serverConstants} from "../_constants";

import {authHeader} from "../_helpers";

export const fileUploadService = {
    uploadFile
};

function uploadFile(file) {
    let data = new FormData();
    data.append('file', file);

    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body: data
    };

    return fetch(`${serverConstants.SERVER_URL}/file`, requestOptions)
        .then(handleResponse)
        .then(data => {
            return data;
        });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            return Promise.reject(data);
        }

        return data;
    });
}
