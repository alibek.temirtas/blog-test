import {fileUploadConstants} from "../_constants";

export function files(state = {}, action) {
    switch (action.type) {

        case fileUploadConstants.CREATE_SUCCESS:
            return {
                file: action.data
            };

        case fileUploadConstants.CREATE_FAILURE:

            return {
                error: action.error
            };

        case fileUploadConstants.CLEAR:
            return {};

        default:
            return state
    }
}
