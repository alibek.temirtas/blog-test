import {userConstants} from '../_constants';

export function users(state = {}, action) {
    switch (action.type) {
        case userConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        case userConstants.GETALL_SUCCESS:
            return {
                pages: action.data.pages,
                items: action.data.users,
                count: action.data.count
            };

        case userConstants.GETALL_FAILURE:

            return {
                error: action.error
            };

        case userConstants.DELETE_REQUEST:

            return {
                ...state,
                items: state.items.map(user =>
                    user.id === action.id
                        ? {...user, deleting: true}
                        : user
                )
            };

        case userConstants.DELETE_SUCCESS:

            return {
                items: state.items.filter(user => user.id !== action.id)
            };

        case userConstants.DELETE_FAILURE:

            return {
                ...state,
                items: state.items.map(user => {
                    if (user.id === action.id) {
                        const {deleting, ...userCopy} = user;
                        return {...userCopy, deleteError: action.error};
                    }

                    return user;
                })
            };

        case userConstants.UPDATE_REQUEST:
            return {
                ...state,
                items: state.items.map(user =>
                    user._id === action.user._id
                        ? {...user, ...action.user}
                        : user
                )
            };

        case userConstants.UPDATE_SUCCESS:
            return {
                ...state,
                items: state.items.map(user =>
                    user._id === action.user._id
                        ? {...user, ...action.user}
                        : user
                )
            };

        case userConstants.UPDATE_FAILURE:
            return {
                ...state,
                items: state.items.map(user => {
                    if (user._id === action.user._id) {
                        return {updateError: action.error};
                    }

                    return user;
                })
            };

        case userConstants.CREATE_USER_SUCCESS:

            return {
                ...state,
                items: [
                    action.user,
                    ...state.items

                ]
            };

        case userConstants.CREATE_USER_FAILURE:

            return {
                ...state,
                error: action.error
            };
        default:
            return state
    }
}
