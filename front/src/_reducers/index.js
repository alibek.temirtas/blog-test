import { combineReducers } from 'redux';

import {authentication} from "./auth.reducer";
import {users} from "./user.reducer";
import {blogs} from "./blog.reducer";
import {comments} from "./comment.reducer";
import {alert} from "./alert.reducer";
import {files} from "./file-upload.reducer";

const rootReducer = combineReducers({
    authentication,
    users,
    blogs,
    comments,
    alert,
    files
});

export default rootReducer;
