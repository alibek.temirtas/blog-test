import {blogConstants} from "../_constants/blog.constants";

export function blogs(state = {}, action) {
    switch (action.type) {
        case blogConstants.CREATE_SUCCESS:
            return {
                blog: action.blog
            };
        case blogConstants.GETALL_SUCCESS:

            return {
                pages: action.data.pages,
                items: action.data.blogs,
                count: action.data.count
            };

        case blogConstants.GETALL_FAILURE:

            return {
                error: action.error
            };

        case blogConstants.GET_BLOGBYID_SUCCESS:
            return {
                blog: action.blog
            };

        case blogConstants.GET_BLOGBYID_FAILURE:

            return {
                error: action.error
            };

        case blogConstants.UPDATE_REQUEST:
            return {
                ...state,
                items: state.items.map(blog =>
                    blog._id === action.blog._id
                        ? {...blog, ...action.blog}
                        : blog
                )
            };

        case blogConstants.UPDATE_SUCCESS:
            return {
                ...state,
                items: state.items.map(blog =>
                    blog._id === action.blog._id
                        ? {...blog, ...action.blog}
                        : blog
                )
            };

        case blogConstants.UPDATE_FAILURE:
            return {
                ...state,
                items: state.items.map(blog => {
                    if (blog._id === action.blog._id) {
                        return {updateError: action.error};
                    }

                    return blog;
                })
            };

        case blogConstants.DELETE_REQUEST:
            return {
                ...state,
                items: state.items.map(blog =>
                    blog._id === action.id
                        ? {...blog, deleting: true}
                        : blog
                )
            };
        case blogConstants.DELETE_SUCCESS:
            return {
                items: state.items.filter(blog => blog._id !== action.id)
            };
        case blogConstants.DELETE_FAILURE:
            return {
                ...state,
                items: state.items.map(blog => {
                    if (blog._id === action.id) {
                        return {deleteError: action.error};
                    }

                    return blog;
                })
            };
        default:
            return state
    }
}
