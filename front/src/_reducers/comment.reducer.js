import {commentConstants} from "../_constants";

export function comments(state = {}, action) {
    switch (action.type) {
        case commentConstants.GETALL_BYBLOGID_SUCCESS:

            return {
                items: action.comments
            };

        case commentConstants.GETALL_BYBLOGID_FAILURE:

            return {
                error: action.error
            };

        case commentConstants.CREATE_SUCCESS:
            return {
                ...state,
                items: [
                    ...state.items,
                    action.comment
                ]
            };

        case commentConstants.PUSH_COMMENT:
            return {
                ...state,
                items: [
                    ...state.items,
                    action.comment
                ]
            };

        case commentConstants.POP_COMMENT:
            return {
                items: state.items.filter(comment => comment._id !== action.id)
            };

        case commentConstants.CREATE_FAILURE:

            return {
                error: action.error
            };

        case commentConstants.DELETE_REQUEST:
            return {
                ...state,
                items: state.items.map(comment =>
                    comment._id === action.id
                        ? {...comment, deleting: true}
                        : comment
                )
            };
        case commentConstants.DELETE_SUCCESS:
            return {
                items: state.items.filter(comment => comment._id !== action.id)
            };
        case commentConstants.DELETE_FAILURE:
            return {
                ...state,
                items: state.items.map(comment => {
                    if (comment._id === action.id) {
                        return {deleteError: action.error};
                    }

                    return comment;
                })
            };
        default:
            return state
    }
}
