import React, {Component} from 'react';
import {Router, Route, Link} from 'react-router-dom';
import {history} from "../_helpers";
import {connect} from 'react-redux';
import {Icon} from "antd";
import {PrivateRoute} from "../Components/PrivateRoute/PrivateRoute";

import {role} from "../_constants/role.constant";

// SASS :
import '../style/_main.scss';

// Components :
import {LoginComponent} from "../Components/Login/login.component";
import {RegisterComponent} from '../Components/Register/register.component';
import {HomeComponent} from '../Components/Home/home.component';
import {AccountComponent} from "../Components/Account/_root/root.component";
import {DetailComponent} from "../Components/Detail/detail.component";

import {AlertModalComponent} from "../Shared/Modals/Alert/alert.modal";

import "antd/dist/antd.css";
import {Col, Row, Menu} from "antd";

class App extends Component {

    render() {
        const {user} = this.props;

        return (
            <div>
                <AlertModalComponent />
                <Row>
                    <Col span={5}></Col>
                    <Col span={12}>
                        <Router history={history}>
                            <Menu onClick={this.handleClick} mode="horizontal">
                                <Menu.Item key="main">
                                    <Link to="/" className="btn btn-link"><Icon type="home"/>Главная</Link>
                                </Menu.Item>

                                <Menu.Item key="register" className="float-right">
                                    {user && user.user ? <Link to="/login" className="btn btn-link"><Icon type="logout"/>Выйти</Link> :
                                            <Link to="/register" className="btn btn-link"><Icon type="user-add"/>Регистрация</Link>
                                    }
                                </Menu.Item>

                                <Menu.Item key="login" className="float-right">
                                    {user && user.user ?
                                        <Link to={user.user.role === role.USER ? '/account/my?status=200' : '/account/admin/users'}
                                              className="btn btn-link">{user.user.profile.name}</Link> :
                                        <Link to="/login" className="btn btn-link"><Icon type="login"/>Войти</Link>
                                    }
                                </Menu.Item>
                            </Menu>
                        </Router>
                    </Col>
                    <Col span={5}></Col>
                </Row>
                <Row>
                    <Col span={5}></Col>
                    <Col span={12}>
                        <Router history={history}>
                            <PrivateRoute path="/account" component={AccountComponent}/>
                            <Route path="/login" component={LoginComponent}/>
                            <Route path="/register" component={RegisterComponent}/>
                            <Route exact path="/" component={HomeComponent}/>
                            <Route exact path="/detail/:id" component={DetailComponent}/>
                        </Router>
                    </Col>
                    <Col span={5}></Col>
                </Row>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {authentication} = state;
    const {user} = authentication;
    return {
        user
    };
}

const connectedApp = connect(mapStateToProps)(App);
export {connectedApp as App};
