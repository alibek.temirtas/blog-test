export function authHeader() {
    let user = JSON.parse(localStorage.getItem('user'));

    console.log('use tokenr: ', user);
    if (user && user.token) {
        return {'Authorization': 'Bearer ' + user.token};
    } else {
        return {};
    }
}
