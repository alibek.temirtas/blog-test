import React, {Component} from 'react';
import {connect} from 'react-redux';
import queryString from 'query-string';

import {blogActions} from "../../../_actions/blog.actions";
import BlogList from "../../../Shared/Components/Blog/blog-list.component";
import {BlogEditModalComponent} from "../../../Shared/Modals/Blog/blog-edit.modal";

class MyBlogsComponent extends Component {
    defaultPageNumber = 1;

    constructor(props) {
        super(props);

        this.state = {
            blog: null,
            visible: false
        };
    }

    componentWillMount() {
        console.log('this.props.location.search: ', this.props.location.search);
        this.props.dispatch(blogActions.getBlogsByUserId(this.defaultPageNumber));
    }

    handleDeleteBlog = (blogId) => {
        this.props.dispatch(blogActions.delete(blogId));
    };

    handleEditBlog = (blog) => {
        this.setState({
            visible: true,
            blog: blog
        });
    };

    handleOk = (e) => {
        this.setState({
            visible: false
        });
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };

    // Pagination
    onChangePage = (pageNumber) => {
        this.props.dispatch(blogActions.getBlogsByUserId(pageNumber));
    };

    render() {
        let {blogs} = this.props;
        let {blog, visible} = this.state;
        const {status} = queryString.parse(this.props.location.search);

        return (
            <React.Fragment>
                <h2 className="no-margin">Мои блоги</h2>

                <BlogList blogs={blogs.items}
                          pages={blogs.pages}
                          defaultPageNumber = {this.defaultPageNumber}
                          deleteBlog={this.handleDeleteBlog}
                          editBlog={this.handleEditBlog}
                          onChangePage={this.onChangePage}
                          extended
                          status={status}/>

                <BlogEditModalComponent blog={blog} visible={visible}
                                        handleOk={this.handleOk} handleCancel={this.handleCancel}/>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    const {blogs} = state;
    return {blogs};
}

const connectedMyBlogsComponent = connect(mapStateToProps)(MyBlogsComponent);
export {connectedMyBlogsComponent as MyBlogsComponent};
