import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Alert, Button, Input} from "antd";

import {blogActions} from "../../../_actions/blog.actions";
import TextArea from "antd/lib/input/TextArea";

import {UploadFileComponent} from "../../../Shared/Components/File/file-upload.component";

class CreateBlogComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            blog: {
                title: null,
                desc: null
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.createBlog.bind(this);
    }

    handleChange(event) {
        const {name, value} = event.target;
        const {blog} = this.state;
        this.setState({
            blog: {
                ...blog,
                [name]: value
            }
        });
    }

    createBlog(e) {
        e.preventDefault();

        const {user} = this.props;
        this.setState({submitted: true});
        const {blog} = this.state;
        const { dispatch, files } = this.props;
        if (blog.title && blog.desc) {
            blog.author = user.user.profile;
            blog.userId = user.user._id;
            blog.filePath = files.file && files.file.path;

            dispatch(blogActions.create(blog));
        }
    }

    render() {
        const {blog, submitted} = this.state;
        return (
            <React.Fragment>
                <h2>Создание блога</h2>
                <div>
                    <form className="form" name="form">

                        <UploadFileComponent />

                        <Input className="form__item" placeholder="Введите заголовок" type="text" name="title"
                               value={blog.title} onChange={this.handleChange}/>
                        {submitted && !blog.title && <Alert message="Заголовок обязательно" type="error"/>}

                        <TextArea className="form__item" placeholder="Введите описание" type="text" name="desc"
                               value={blog.desc} onChange={this.handleChange}/>
                        {submitted && !blog.desc && <Alert message="Описание обязательно" type="error"/>}

                        <Button type="primary" onClick={this.handleSubmit}>Подтвердить</Button>

                    </form>
                </div>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    const {authentication, files} = state;
    const {user} = authentication;
    return {
        user,
        files
    };
}

const connectedCreateBlogComponent = connect(mapStateToProps)(CreateBlogComponent);
export {connectedCreateBlogComponent as CreateBlogComponent};
