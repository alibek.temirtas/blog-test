import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Router, Route} from 'react-router-dom';

import {Row, Col} from "antd";
import {history} from "../../../_helpers";

import {role} from "../../../_constants/role.constant";

import {CreateBlogComponent} from "../create-blog/create-blog.component";
import {MyBlogsComponent} from "../my-blogs/my-blogs.component";

// Admin panel
import {PrivateRouteAdmin} from "../../PrivateRoute/admin.private";
import {AdminUsersComponent} from "../../Admin/users/users.component";
import {AdminBlogsComponent} from "../../Admin/blogs/blogs.component";

import AdminMenuComponent from "../../Admin/menu/admin-menu.component";
import UserMenuComponent from  "../../Account/menu/user-menu.component";

class RootComponent extends Component {

    componentWillMount() {
    }

    render() {
        const userRole = this.props.user.user && +this.props.user.user.role;
        return (
            <React.Fragment>
                <Row>
                    <Col span={8}>
                        { userRole === role.USER && <UserMenuComponent />}
                        { userRole === role.ADMIN && <AdminMenuComponent />}
                    </Col>
                    <Col span={15}>
                        <div className="router_wr">
                            <Router history={history}>
                                <Route activeClassName='is-active' exact path={`${this.props.match.url}/create`} component={CreateBlogComponent}/>
                                <Route activeClassName='is-active' exact path={`${this.props.match.url}/my`} component={MyBlogsComponent}/>

                                <PrivateRouteAdmin exact path={`${this.props.match.url}/admin/users`} component={AdminUsersComponent}/>
                                <PrivateRouteAdmin exact path={`${this.props.match.url}/admin/blogs`} component={AdminBlogsComponent}/>
                            </Router>
                        </div>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    const {authentication} = state;
    const {user} = authentication;
    return {
        authentication,
        user
    };
}

const connectedAccountComponent = connect(mapStateToProps)(RootComponent);
export {connectedAccountComponent as AccountComponent};
