import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import {Menu, Icon} from "antd";

const SubMenu = Menu.SubMenu;

class UserMenuComponent extends Component {

    componentWillMount() {
    }

    render() {
        return (
            <React.Fragment>
                <Menu
                    defaultSelectedKeys={['approved']}
                    defaultOpenKeys={['approved']}
                    mode="inline">
                    <Menu.Item key="create">
                        <Link to={`/account/create`}><Icon type="plus"/><span>Создать новый блог</span></Link>
                    </Menu.Item>
                    <SubMenu key="blogs" title={<span><Icon type="copy"/><span>Мои блоги</span></span>}>
                        <Menu.Item key="approved">
                            <Link to={`/account/my?status=200`}>Активные</Link>
                        </Menu.Item>
                        <Menu.Item key="moderation">
                            <Link to={`/account/my?status=100`}>На модерации</Link>
                        </Menu.Item>
                        <Menu.Item key="rejected">
                            <Link to={`/account/my?status=99`}>Отказано</Link>
                        </Menu.Item>
                    </SubMenu>

                </Menu>
            </React.Fragment>
        );
    }
}

export default UserMenuComponent;
