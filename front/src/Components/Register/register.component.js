import React, {Component} from 'react';

import {Button, Input, Alert, Col, Modal} from 'antd';

import {userService} from "../../_services/user.service";
import {Link} from "react-router-dom";

import {alertActions} from "../../_actions/alert.actions";
import {connect} from "react-redux";

class RegisterComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                phoneNumber: null,
                password: null,
                name: null,
                surname: null
            },
            submitted: false,
            activateCode: null,
            visible: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeActivate = this.handleChangeActivate.bind(this);
        this.handleSubmit = this.registerUser.bind(this);
        this.handleSubmitActivate = this.activateCode.bind(this);
    }

    handleChange(event) {
        const {name, value} = event.target;
        const {user} = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    registerUser(e) {
        e.preventDefault();
        const {user} = this.state;
        const {dispatch} = this.props;

        this.setState({submitted: true});
        if (user.phoneNumber && user.password) {
            userService.register(user).then((data) => {
                this.setState({
                    visible: true
                });
            }, error => {
                dispatch(alertActions.error('Данный номер телефона уже зарегистрирован'));
            });
        }
    }

    handleCancel=()=>{
        this.setState({
            visible: false
        });
    };

    activateCode(e) {
        e.preventDefault();
        const {activateCode} = this.state;
        const {dispatch} = this.props;

        let formData = {
            phoneNumber: this.state.user.phoneNumber,
            activateCode: activateCode
        };

        if (formData) {
            userService.activate(formData).then((data) => {
                dispatch(alertActions.success('Вы успешно зарекистрированы.'));
                this.setState({
                    visible: false
                });
            }, error => {
                this.setState({
                    visible: false
                });
                dispatch(alertActions.error('Ошибочка.'));
            });
        }
    }

    handleChangeActivate(event) {
        const {value} = event.target;
        this.setState({
            activateCode: value
        });
    }

    render() {
        const {user, submitted, visible} = this.state;
        const {activateCode} = this.state;

        return (
            <div>
                <Col span={5}></Col>
                <Col span={12}>
                    <h2>Регистрация</h2>
                    <form className="form" name="form" >

                        <Input className="form__item" placeholder="Введите номер телефона" type="number" name="phoneNumber"
                               value={user.phoneNumber} onChange={this.handleChange}/>
                        {submitted && !user.phoneNumber && <Alert message="Номер телефона обязательно" type="error"/>}

                        <Input className="form__item" placeholder="Введите пароль" type="password" name="password"
                               value={user.password} onChange={this.handleChange}/>
                        {submitted && !user.password && <Alert message="Пароль обязательно" type="error"/>}

                        <Input className="form__item" placeholder="Введите имя" type="text" name="name" value={user.name}
                               onChange={this.handleChange}/>

                        <Input className="form__item" placeholder="Введите фамилию" type="text" name="surname"
                               value={user.surname} onChange={this.handleChange}/>

                        <Button type="primary" onClick={this.handleSubmit}>Подтвердить</Button>
                        <Button type="primary" className="float-right"><Link to="/login"
                                                                             className="btn btn-link">Назад</Link></Button>

                    </form>

                    <Modal title="Код активации личного кабинета"
                           visible={visible}
                           onOk={this.handleSubmitActivate}
                           onCancel={this.handleCancel}>

                        <form className="form" name="form" onSubmit={this.handleSubmitActivate}>
                            <Input className="form__item" placeholder="Введите код" type="text" name="activateCode"
                                   value={activateCode} onChange={this.handleChangeActivate}/>

                        </form>
                    </Modal>
                </Col>
                <Col span={5}></Col>

            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedRegisterComponent = connect(mapStateToProps)(RegisterComponent);
export { connectedRegisterComponent as RegisterComponent };
