import React, {Component} from 'react';
import {Link, Redirect} from "react-router-dom";
import { connect } from 'react-redux';

import {Alert, Button, Input, Col, Row} from "antd";

import {userActions} from "../../_actions";

class LoginComponent extends Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            user: {
                phoneNumber: null,
                password: null
            },
            submitted: false,
            redirect: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.login.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    login(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        const { dispatch } = this.props;
        if (user.phoneNumber && user.password) {
            dispatch(userActions.login(user.phoneNumber, user.password));
        }
    }

    render() {
        if (this.state.redirect) {
            return <Redirect push to="/" />;
        }
        const { user, submitted } = this.state;
        return (
            <div>
                <Row>
                    <Col span={5}></Col>
                    <Col span={12}>
                        <h2>Авторизация</h2>
                        <form className="form" name="form">

                            <Input className="form__item" placeholder="Введите номер телефона" type="number" name="phoneNumber" value={user.phoneNumber} onChange={this.handleChange} />
                            {submitted && !user.phoneNumber && <Alert message="Номер телефона обязательно" type="error" />}

                            <Input className="form__item" placeholder="Введите пароль" type="password" name="password" value={user.password} onChange={this.handleChange} />
                            {submitted && !user.password &&  <Alert message="Пароль обязательно" type="error" />}

                            <Button type="primary" onClick={this.handleSubmit}>Войти</Button>
                            <Button className="float-right" type="primary" ><Link to="/register" className="btn btn-link">Регистрация</Link></Button>

                        </form>
                    </Col>
                    <Col span={5}></Col>
                </Row>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginComponent);
export { connectedLoginPage as LoginComponent };
