import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Breadcrumb, Button, Empty, Icon} from "antd";
import {blogActions} from "../../_actions/blog.actions";
import {history} from "../../_helpers";

import BlogDetailComponent from '../../Shared/Components/Blog/blog-detail.component';
import {Link} from "react-router-dom";

class DetailComponent extends Component {

    componentWillMount() {
        const {id} = this.props.match.params;
        const {user} = this.props;
        if(user){
            this.props.dispatch(blogActions.getBlogById(id));
        }
    }

    render() {
        const {blog} = this.props;
        return (
            <React.Fragment>
                <Breadcrumb>
                    <Breadcrumb.Item><span className="cursor-pointer" onClick={history.goBack}>Вернуться назад</span></Breadcrumb.Item>
                    <Breadcrumb.Item>Блог</Breadcrumb.Item>
                </Breadcrumb>
                <BlogDetailComponent blog={blog}/>

                {!blog &&
                <Empty
                    image="/assets/img/original.png"
                    description={
                        <span>
                            Для подробного просмотра авторизуйтесь
                          </span>}>

                    <Link to="/login" className="btn btn-link"><Button type="primary"><Icon
                        type="login"/> Войти</Button></Link>

                </Empty>
                }
            </React.Fragment>
        );
    }

}

function mapStateToProps(state) {
    const {authentication, blogs} = state;
    const {user} = authentication;
    const {blog} = blogs;
    return {
        blog,
        user
    };
}

const connectedDetailComponent = connect(mapStateToProps)(DetailComponent);
export {connectedDetailComponent as DetailComponent};
