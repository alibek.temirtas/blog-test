import React, {Component} from 'react';
import {connect} from 'react-redux';

import {blogActions} from "../../../_actions/blog.actions";

import {BlogEditModalComponent} from "../../../Shared/Modals/Blog/blog-edit.modal";
import BlogList from '../../../Shared/Components/Blog/blog-list.component';

class AdminBlogsComponent extends Component {
    defaultPageNumber = 1;

    constructor(props) {
        super(props);

        this.state = {
            blog: null,
            visible: false
        };

    }

    componentWillMount() {
        this.props.dispatch(blogActions.getAllBlogForAdmin(this.defaultPageNumber));
    }

    handleDeleteBlog = (blog) => {
        blog.isDeleted = true;
        this.props.dispatch(blogActions.updateBlog(blog));
    };

    handleApproveBlog = (blog) => {
        blog.status = 200;
        this.props.dispatch(blogActions.updateBlog(blog));
    };

    handleRejectBlog = (blog) => {
        blog.status = 99;
        this.props.dispatch(blogActions.updateBlog(blog));
    };

    handleRecoverBlog = (blog) => {
        blog.isDeleted = false;
        this.props.dispatch(blogActions.updateBlog(blog));
    };

    handleEditBlog = (blog) => {
        this.setState({
            visible: true,
            blog: blog
        });
    };

    handleOk = (e) => {
        this.setState({
            visible: false
        });
    };

    handleCancel = (e) => {
        this.setState({
            visible: false
        });
    };

    // Pagination
    onChangePage = (pageNumber) => {
        this.props.dispatch(blogActions.getAllBlogForAdmin(pageNumber));
    };

    render() {
        const {blogs} = this.props;
        const {blog, visible} = this.state;
        return (
            <div>
                <h3>Все блоги {blogs.count && `(${blogs.count})`}</h3>
                <BlogList blogs={blogs.items}
                          pages={blogs.pages}
                          defaultPageNumber = {this.defaultPageNumber}
                          deleteBlog={this.handleDeleteBlog}
                          editBlog={this.handleEditBlog}
                          recoverBlog={this.handleRecoverBlog}
                          approveBlog={this.handleApproveBlog}
                          rejectBlog={this.handleRejectBlog}
                          onChangePage={this.onChangePage}
                          extended
                          adminPanel/>
                <BlogEditModalComponent blog={blog} visible={visible}
                                        handleOk={this.handleOk} handleCancel={this.handleCancel}/>
            </div>
        );
    }

}

function mapStateToProps(state) {
    const {blogs} = state;
    return {
        blogs
    };
}

const connectedAdminBlogsComponent = connect(mapStateToProps)(AdminBlogsComponent);
export {connectedAdminBlogsComponent as AdminBlogsComponent};
