import React, {Component} from 'react';
import {connect} from 'react-redux';

import {userActions} from "../../../_actions";
import {Button, Icon} from "antd";

import {UserCreateModalComponent} from "../../../Shared/Modals/User/user-create.modal";
import {UserEditModalComponent} from "../../../Shared/Modals/User/user-edit.modal";

import UserList from "../../../Shared/Components/User/user-list.component";

class AdminUsersComponent extends Component {
    defaultPageNumber = 1;

    constructor(props) {
        super(props);

        this.state = {
            user: null,
            visible: false,
            submitted: false,
            visibleCreate: false
        };

    }

    componentWillMount() {
        this.props.dispatch(userActions.getAll(this.defaultPageNumber));
    }

    handleDeleteUser = (user) => {
        user.isDeleted = true;
        this.props.dispatch(userActions.updateUser(user));
    };

    handleRecoverUser = (user) => {
        user.isDeleted = false;
        this.props.dispatch(userActions.updateUser(user));
    };


    handleEditUser = (user) =>{
        this.setState({
            visible: true,
            user: user
        });
    };

    handleOk = (e) => {
        this.setState({
            visible: false
        });
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    };


    // Manipulation User create modal
    handleOkCreateUser = (e) => {
        this.setState({
            visibleCreate: false
        });
    };

    handleCancelCreateUser = (e) => {
        this.setState({
            visibleCreate: false
        });
    };

    openModalCreateUser() {
        this.setState({
            visibleCreate: true
        });
    }

    // Pagination
    onChangePage = (pageNumber) => {
        this.props.dispatch(userActions.getAll(pageNumber));
    };

    render() {
        const {users} = this.props;
        const {user, visible, visibleCreate} = this.state;

        return (
            <React.Fragment>
                <h3>Все пользователи {users.count && `(${users.count})`}</h3>
                <Button className="margin-bottom" type="default" onClick={this.openModalCreateUser.bind(this)}><Icon type="plus"></Icon> Создать пользователя</Button>

                <UserList users = {users}
                          defaultPageNumber = {this.defaultPageNumber}
                          editUser={this.handleEditUser}
                          deleteUser={this.handleDeleteUser}
                          recoverUser={this.handleRecoverUser}
                          onChangePage={this.onChangePage}/>



                <UserEditModalComponent user={user} visible={visible}
                                        onOk={this.handleOk}
                                        onCancel={this.handleCancel}/>

                <UserCreateModalComponent visible={visibleCreate}
                                          onOk={this.handleOkCreateUser}
                                          onCancel={this.handleCancelCreateUser}/>
            </React.Fragment>
        );
    }

}

function mapStateToProps(state) {
    const {users} = state;
    return {
        users
    };
}

const connectedAdminUsersComponent = connect(mapStateToProps)(AdminUsersComponent);
export {connectedAdminUsersComponent as AdminUsersComponent};
