import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import {Menu, Icon} from "antd";;

class AdminMenuComponent extends Component {

    componentWillMount() {
    }

    render() {
        return (
            <React.Fragment>
                <Menu
                    defaultSelectedKeys={['users']}
                    defaultOpenKeys={['users']}
                    mode="inline">
                    <Menu.Item key="users">
                        <Link to={`/account/admin/users`}><Icon type="user"/><span>Пользователи</span></Link>
                    </Menu.Item>
                    <Menu.Item key="blogs">
                        <Link to={`/account/admin/blogs`}><Icon type="copy"/><span>Блоги</span></Link>
                    </Menu.Item>
                </Menu>
            </React.Fragment>
        );
    }
}

export default AdminMenuComponent;
