import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import {role} from "../../_constants/role.constant";

export const PrivateRouteAdmin = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        JSON.parse(localStorage.getItem('user')).user.role === role.ADMIN
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
);
