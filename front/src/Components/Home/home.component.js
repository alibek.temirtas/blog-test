import React, {Component} from 'react';
import {connect} from 'react-redux';

import {blogActions} from "../../_actions/blog.actions";
import BlogList from "../../Shared/Components/Blog/blog-list.component";

class HomeComponent extends Component {
    defaultPageNumber = 1;

    componentWillMount() {
        this.props.dispatch(blogActions.getAll(this.defaultPageNumber));
    }

    // Pagination
    onChangePage = (pageNumber) => {
        this.props.dispatch(blogActions.getAll(pageNumber));
    };

    render() {
        const {blogs} = this.props;
        return (
            <React.Fragment>
                <div className="outline">
                    <BlogList blogs={blogs.items}
                              pages={blogs.pages}
                              defaultPageNumber = {this.defaultPageNumber}
                              onChangePage={this.onChangePage}/>
                </div>
            </React.Fragment>
        );
    }

}

function mapStateToProps(state) {
    const {blogs} = state;
    return {
        blogs
    };
}

const connectedHomeComponent = connect(mapStateToProps)(HomeComponent);
export {connectedHomeComponent as HomeComponent};
